function image_luminosity = calibrate_image (image_luminosity, dark_frame, flat_field, bad_pixel_map)

image_luminosity = image_luminosity(1:2054,1:3086) - dark_frame;
image_luminosity = image_luminosity ./ flat_field;

median_frame = median_x_dll (image_luminosity, 1, 1);

image_luminosity(bad_pixel_map > .1) = 0;