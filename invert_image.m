function image_out = invert_image(image_in)

scale = 2^16;
min_image = min(image_in(:));
max_image = max(image_in(:));
image_out = (image_in - min_image)/(max_image - min_image)*scale;
image_out = scale - image_out;