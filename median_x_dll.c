#include "mex.h"

#define NR_END 1

static float stemp;
#define SWAP(a, b) stemp = (a); (a) = (b); (b) = stemp;
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#define MIN(x,y) ((x) < (y) ? (x) : (y))

//#define DEBUG

#ifdef DEBUG
void main()
{
#else
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
#endif

	float select(unsigned long k, unsigned long n, float arr[]);
	float *fvector(long nl, long nh);
	void free_fvector(float *v, long nl, long nh);

	int i_row, k_row, n_rows, j_col, l_col, n_cols, row_start, row_end;
	int col_start, col_end, col_skip, kernel_outer_radius, kernel_inner_radius, M;
	int base_point, i_kernel, arm_point;
	long n_pixels, kernel_area, kernel_count;
 	float *kernel_vector;
	bool bEven;

//===================================================================================
//==
#ifdef DEBUG
	double image_luminosity[25000], out_array[25000];
	
	n_rows = 100;
	n_cols = 250;
	
	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {
			image_luminosity[(i_row*n_cols)+j_col] = (double)((i_row*n_cols)+j_col);
		}
	}

	//-- Get matrix x
	n_cols = 75;
	n_rows = 50;
	n_pixels = n_rows * n_cols;
	kernel_outer_radius = 30;
	kernel_inner_radius = 5;
#else
	double *image_luminosity, *out_array;

	//-- Get matrix x
	image_luminosity = mxGetPr (prhs[0]);
	n_rows = mxGetN (prhs[0]);
	n_cols = mxGetM (prhs[0]);
	n_pixels = n_rows * n_cols;
	kernel_outer_radius = (int)(mxGetScalar(prhs[1]));
	kernel_inner_radius = (int)(mxGetScalar(prhs[2]));

	//-- Allocate memory and assign output pointer
	plhs[0] = mxCreateDoubleMatrix(n_cols, n_rows, mxREAL);

	//-- Get a pointer to the data space in the newly allocated memory
	out_array = mxGetPr(plhs[0]);
#endif
//==
//===================================================================================

	//-- some validity checks on the parameters
	if (kernel_inner_radius < 1) 
		kernel_inner_radius = 1;
	if (kernel_outer_radius < kernel_inner_radius) 
		kernel_outer_radius = kernel_inner_radius;

	//-- allocate memory for the square around each pixel
	//-- use Numerical Recipes allocation routine because kernel_vector[]
	//-- will be passed to select(), which takes the input array as arr[1..nl]
	kernel_area = 8*(kernel_outer_radius - kernel_inner_radius + 1);
	kernel_vector = fvector(1, kernel_area);

	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {

			//-- Here is the central pixel of the '*' pattern
			base_point =  i_row*n_cols + j_col;
			kernel_count = 0;

			//-- Here is the '*' pattern around the central pixel
			//-- Use caution to only store pixels that are within the 
			//-- image frame; make limit checks for each point
			for (i_kernel=kernel_inner_radius; i_kernel<=kernel_outer_radius; i_kernel++) {

				//-- there must be 4 limit checks for each point
				//-- left/right and up/down. Pixels must be within
				//-- the image
				row_start = (i_row - i_kernel)*n_cols;
				row_end = (i_row - i_kernel + 1)*n_cols - 1;

				//-- Upper-Left Branch
				arm_point = base_point - i_kernel*(n_cols + 1);
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Upper-Center Branch
				arm_point = base_point - i_kernel*n_cols;
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Upper-Right Branch
				arm_point = base_point - i_kernel*(n_cols - 1);
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Left-Center Branch
				arm_point = base_point - i_kernel;
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				row_start = (i_row + i_kernel)*n_cols;
				row_end = (i_row + i_kernel + 1)*n_cols - 1;

				//-- Lower-Left Branch
				arm_point = base_point + i_kernel*(n_cols - 1);
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Lower-Center Branch
				arm_point = base_point + i_kernel*n_cols;
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Lower-Right Branch
				arm_point = base_point + i_kernel*(n_cols + 1);
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
				//-- Right-Center Branch
				arm_point = base_point + i_kernel;
				if (((arm_point >= row_start)  && (arm_point <= row_end)) &&
				    ((arm_point > 0) && (arm_point < n_pixels - 1)))
					kernel_vector[++kernel_count] = image_luminosity[arm_point];
	
			}

			bEven = !(kernel_count % 2);

			M = kernel_count;
			M >>= 1;
			if (!bEven) M++;
			out_array[(i_row*n_cols)+j_col] = (double) select(M, kernel_count, kernel_vector);

			if (bEven) { // if even, take the average of Mth and (M+1)th
				out_array[(i_row*n_cols)+j_col] = (double) (out_array[(i_row*n_cols)+j_col] + select(M + 1, kernel_count, kernel_vector)) / 2.0;
			}
	
		}
	}

	free_fvector (kernel_vector, 1, kernel_area);

	return;
}

//==========================================================================
//
// Function: float select
//
// Purpose : Numerical Recipes Function, Select kth largest member of an
//           array with subscript range arr[1..n]
//           modified to inline float
//
//==========================================================================
float select(unsigned long k, unsigned long n, float arr[])
{
	unsigned long i, ir, j, l, mid;
	float a;

	l = 1;
	ir = n;
	for (;;) {
		if (ir <= l + 1) {
			if (ir == l+1 && arr[ir] < arr[l]) {
				SWAP(arr[l], arr[ir])
			}
			return arr[k];
		} else {
			mid = (l + ir) >> 1;
			SWAP(arr[mid], arr[l + 1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l], arr[ir])
			}
			if (arr[l + 1] > arr[ir]) {
				SWAP(arr[l + 1], arr[ir])
			}
			if (arr[l] > arr[l + 1]) {
				SWAP(arr[l], arr[l + 1])
			}
			i = l + 1;
			j = ir;
			a = arr[l + 1];
			for (;;) {
				//-- is this a bug in Numerical Recipes code?
				//-- for certain data sets, the j index goes
				//-- below one and keeps going???
				//do i++; while (arr[i] < a);
				//do j--; while (arr[j] > a);
				do i++; while ((arr[i] < a) && (i<n));
				do j--; while ((arr[j] > a) && (j>1));
				if (j < i) break;
				SWAP(arr[i], arr[j])
			}
			arr[l + 1] = arr[j];
			arr[j] = a;
			if (j >= k) ir = j-1;
			if (j <= k) l = i;
		}
	}
}

//==========================================================================
//
// Function: int *fvector
// Function: void free_fvector
//
// Purpose : Numerical Recipes Function, allocate a float vector 
//           with subscript range v[nl..nh]
//           from nrutil.c
//
//==========================================================================
float *fvector(long nl, long nh)
{
	float *v;
 	char errorMsg[512];

	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	if (!v) {
		return NULL;
	}
	return (v+NR_END-nl);
}

void free_fvector(float *v, long nl, long nh)
{
	nh;
	free((char*) (v+nl-NR_END));
}

#undef SWAP
#undef MIN
#undef MAX
#undef NR_END
