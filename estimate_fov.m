function [ra_range, dec_range] = estimate_fov (year, month, day, hour, minute, seconds, station_info)

% The declination range will be a constant, which is based on the station latitude
% that is read in from the Station Info file.  This is passed in to this function
% in the struct as station_info.station_lat
% Apply a correction to the station latitude based on past error between 
% calculated declination and observed declination; this will give the nominal
% location of the center of the image
% dec_correct_degrees = 0.5038; % correction for DECEMBER 2007 data
dec_correct_degrees = -2.5261;  %correction for MARCH 2008 data
base_lat_degrees = station_info.station_lat + dec_correct_degrees;

% From the image center, the upper and lower edges of the image are determined 
% by the number of rows in the image, i.e., the number of pixels in the y-direction,
% and the plate scale of the telescope system.  Since there will be some uncertainty
% in where the telescope is pointing, an extended border around the nominal field
% of view is added to the half-range.
half_range_degrees = ((station_info.n_pixels_y/2) + station_info.fov_border_pixels)*station_info.pixel_pitch_y;
dec_range(1) = base_lat_degrees - half_range_degrees;
dec_range(2) = base_lat_degrees + half_range_degrees;

% The TOSS Matlab programs will read the .crw files directly using dcraw and readraw.m.
% The right ascension will be calculated from the time the image exposure
% was started.  The telescope is pointing at the zenith when the exposure
% begins, and the time that the exposure begins is coded into the image
% filename, e.g., 07_02_0913796.crw means the exposure for the image began
% at 07hr 02min 09.13796minutes UT with the telescope pointed at zenith.
% The UT is converted to LST by an algorithm in Curtis.  Then, for
% objects at zenith, RA is the same as LST:
degrees_ra = LST (year, month, day, hour, minute, seconds, station_info.station_lon);

% apply a correction based on past error between calculated RA and observed RA
% ra_correct_degrees = 18.0513; % correction for DECEMBER 2007 data
ra_correct_degrees = 17.1062;   %correction for MARCH 2008 data
degrees_ra = degrees_ra + ra_correct_degrees;

% pull the corrected degrees_ra into the range 0 to 360
if (degrees_ra < 0) | (degrees_ra > 360)
    degrees_ra = degrees_ra - (fix (degrees_ra/360) - 1)*360;
end 

% Find the approximate RA range spanned by the image, assuming the 
% degrees RA found above is at the center of the image, and using the
% width of the image in pixels.  The plate scale in declination is
% a constant value independent of latitude or longitude.  However, the
% plate scale in degrees RA will depend on latitude, so take the nominal
% plate scale in declination and divide by cos(latitude) to determine the
% correct plate scale in degrees RA
pixel_pitch_x = station_info.pixel_pitch_y / abs (cos (base_lat_degrees*pi/180));
half_range_degrees = ((station_info.n_pixels_x/2) + station_info.fov_border_pixels)*pixel_pitch_x;

% the resulting range in RA may span 0 or 360; this must be considered
% by any routines that use the RA range.
ra_range(1) = degrees_ra - half_range_degrees;
ra_range(2) = degrees_ra + half_range_degrees;

% for debug with Castor Frame
% Castor is at 07h 34m 36s, 31d 53m 18s
% ra_error = ra_range - [112.8505 114.4496]
% dec_error = dec_range - [31.3270 32.4490]
% ra_range = [112.8505 114.4496];
% dec_range = [31.3270 32.4490];

% for debug, use Aldebaran frame
% Aldebaran is at 04h 35m 55.2s, 16d 30m 33s
% Frame size is 2218arcsec RA by 1478.7arcsec Dec
% 2218arcsec = 35m 58s RA
% 1478.7arcsec = 24m 38.7s Dec
% ra_range(1) = (4 + 35/60 + 55.2/3600)*15 - (36/60 + 58/3600)*1.2;
% ra_range(2) = (4 + 35/60 + 55.2/3600)*15 + (36/60 + 58/3600)*.5;
% 
% dec_range(1) = (16 + 30/60 + 33/3600) - (24/60 + 38.7/3600)*1;
% dec_range(2) = (16 + 30/60 + 33/3600) + (24/60 + 38.7/3600)*.75;

% for debug, use Triangulum frame
% SAO55306 is at 02h 09m 32.627s, 34d 59m 14.269s
% Frame size is 2218arcsec RA by 1478.7arcsec Dec
% 2218arcsec = 35m 58s RA
% 1478.7arcsec = 24m 38.7s Dec
% ra_range(1) = (2 + 9/60 + 32.627/3600)*15 - (36/60 + 58/3600)*1.5;
% ra_range(2) = (2 + 9/60 + 32.627/3600)*15 + (36/60 + 58/3600)*1.5;
% 
% dec_range(1) = (34 + 59/60 + 14.269/3600) - (24/60 + 38.7/3600)*1.25;
% dec_range(2) = (34 + 59/60 + 14.269/3600) + (24/60 + 38.7/3600)*1.25;
% 
% ra_range(1) = 32.37;
% ra_range(2) = 33.;
% 
% dec_range(1) = 34.6666666666666;
% dec_range(2) = 35.1666666666666;

