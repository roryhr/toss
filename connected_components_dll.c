#include "mex.h"
#include "matrix.h"
#define SEARCH_SIZE  1000000

//#define DEBUG

#ifdef DEBUG
void main()
{
#else
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
#endif

	int i, j, k, l, i_row, n_rows, j_col, n_cols, n_labels, index, i_eq_class;
	int n_valid_labels, i_label;
	int upper_left_label, upper_middle_label, upper_right_label, middle_left_label;
	int *connected_components, n_eq_classes;
	int *equivalence_classes_1, *equivalence_classes_2, *nf, *nf_count;
	int min_star_pixels, max_star_pixels;
	long n_pixels;

//===================================================================================
//==
#ifdef DEBUG
	float *anomaly;
	double dummy;
 	FILE *fp_in;
	
	n_rows = 2054;
	n_cols = 3086;
	min_star_pixels = 8;
	max_star_pixels = 50000;
	n_pixels = (long) n_rows * (long) n_cols;
	anomaly = (float *)malloc((size_t) (n_pixels*sizeof(float)));

	//-- Read in an actual data set from a TOSS image
	fp_in = fopen ("C:\\1work\\Misc\\TOSS\\release\\toss.programs\\data\\debug_data\\06_23_5410360.fit_binary_anomaly.csv","r");
	for (i_row = 0; i_row < n_rows; i_row++)
		for (j_col = 0; j_col < n_cols; j_col++)
			fscanf (fp_in, "%f", &anomaly[(i_row*n_cols)+j_col]);
	fclose (fp_in);
	
#else
	double *anomaly, *out_array1, *out_scalar;

	//-- Get input data array
	anomaly = mxGetPr (prhs[0]);
	n_rows = mxGetN (prhs[0]);
	n_cols = mxGetM (prhs[0]);

	//-- Get input parameters for star criteria
	min_star_pixels = (int)(mxGetScalar(prhs[1]));
	max_star_pixels = (int)(mxGetScalar(prhs[2]));
#endif
//==
//===================================================================================
	
	//-- Allocate temporary space for the connected_components[] array
	n_pixels = (long) n_rows * (long) n_cols;
	connected_components=(int *)malloc((size_t) (n_pixels*sizeof(int)));
	equivalence_classes_1 = (int *)malloc((size_t) ((SEARCH_SIZE+1)*sizeof(int)));
	equivalence_classes_2 = (int *)malloc((size_t) ((SEARCH_SIZE+1)*sizeof(int)));
	nf = (int *)malloc((size_t) ((SEARCH_SIZE+1)*sizeof(int)));
	nf_count = (int *)malloc((size_t) ((SEARCH_SIZE+1)*sizeof(int)));

	//-- Initialize the connected components arrays
	for (i_row = 0; i_row < n_rows; i_row++)
	    for (j_col = 0; j_col < n_cols; j_col++)
			connected_components[(i_row*n_cols)+j_col] = 0;

	n_labels = 0;
	n_eq_classes = 0;
	
	for (i = 0; i <= SEARCH_SIZE; i++) nf_count[i] = 0;
	
	//-- Begin Connected Component Labeling algorithm.  Since the anomaly[] array is
	//-- non-zero only where an object exists, a simple two-pass algorithm should be
	//-- sufficient to label all connected components in the desired way
	for (i_row = 0; i_row < n_rows; i_row++) {
	    for (j_col = 0; j_col < n_cols; j_col++) {

			//-- Skip zero-valued pixels
			index = (i_row*n_cols)+j_col;
			if (anomaly[index] < 0.1)
				continue;
			
			//-- When a non-zero pixel is found, check if there are
			//-- adjacent non-zero pixels that have already been labeled.
			//-- Make sure that the adjacent pixel is within the image.
			
			//-- Upper Left
			if ((i_row > 0) && (j_col > 0)) {
				upper_left_label = connected_components[(i_row - 1) * n_cols + j_col - 1];
			} else {
				upper_left_label = 0;
			}
			
			//-- Upper Middle
			if (i_row > 0) {
				upper_middle_label = connected_components[(i_row - 1) * n_cols + j_col];
			} else {
				upper_middle_label = 0;
			}
			
			//-- Upper Right
			if ((i_row > 0) && (j_col < n_cols -1)) {
				upper_right_label = connected_components[(i_row - 1) * n_cols + j_col + 1];
			} else {
				upper_right_label = 0;
			}

			//-- Left Middle
			if (j_col > 0) {
				middle_left_label = connected_components[i_row * n_cols + j_col - 1];
			} else {
				middle_left_label = 0;
			}

			//-- Implement the connected component labelling algorithm, first-pass logic
			//-- If Upper Left Pixel is labelled, copy that label to the current pixel
			if (upper_left_label) {
				connected_components[index] = upper_left_label;
				continue;
			}

			//-- If arrive here, Upper Left Pixel is not labelled.
			if (!(middle_left_label) && !(upper_middle_label)) {
				//-- If neither middle pixel is labelled, check the upper right pixel
				if (upper_right_label) {
					connected_components[index] = upper_right_label;
				} else {
					//-- If none of the other three pixels are labelled, increment the label and
					//-- store the new label in the current pixel.
					n_labels++;
					connected_components[index] = n_labels;
				}
				continue;
			}

			//-- If arrive here, Upper Left Pixel is not labelled, and at least one
			//-- of the two middle pixels are labelled.
			if ((middle_left_label) && !(upper_middle_label)) {
				connected_components[index] = middle_left_label;
				continue;
			}

			if (!(middle_left_label) && (upper_middle_label)) {
				connected_components[index] = upper_middle_label;
				continue;
			}

			//-- If arrive here, Upper Left Pixel is not labelled, and both
			//-- of the two middle pixels are labelled.  This is a situation
			//-- where equivalent classes may be created
			if ((middle_left_label) && (upper_middle_label)) {
				if (middle_left_label == upper_middle_label) {
					connected_components[index] = middle_left_label;
				} else {
					if (middle_left_label < upper_middle_label) {
						connected_components[index] = middle_left_label;
						
						//-- record equivalence of classes
						n_eq_classes++;
						equivalence_classes_1[n_eq_classes] = upper_middle_label;
						equivalence_classes_2[n_eq_classes] = middle_left_label;

					} else {
						connected_components[index] = upper_middle_label;
						
						//-- record equivalence of classes
						n_eq_classes++;
						equivalence_classes_1[n_eq_classes] = middle_left_label;
						equivalence_classes_2[n_eq_classes] = upper_middle_label;
					}
				}
			}

	    }	//-- end for (j_col = ...
	}	//-- end for (i_row = ...

	//-- The equivalence classes were stored as they were encountered during the 
	//-- first pass.  This approach allows the possibility of transitive classes,
	//-- i.e., that one class could be equivalent to more than one other class,
	//-- e.g., 4 -> 3, and 4 -> 2.  In this case, it is also true that 3 -> 2,
	//-- and the 4 -> 2 equivalence is not useful.
	//-- Here, search the equivalence_classes[] list and resolve all transitive
	//-- associations.  Algorithm from Numerical Recipes, 8.6
	if (n_eq_classes > 0) {
		
		//-- a memory overflow could occur here, so try to abort gracefully
		if (n_labels > SEARCH_SIZE) {
			
			//-- memory cleanup
			free (connected_components);
			free (equivalence_classes_1);
			free (equivalence_classes_2);
			free (nf);
			free (nf_count);

			#ifndef DEBUG			
			//-- Allocate memory and assign output pointer
			plhs[0] = mxCreateScalarDouble (0.0);
			plhs[1] = mxCreateScalarDouble (0.0);

			//-- Get a pointer to the data space in the newly allocated memory
			out_array1 = mxGetPr(plhs[0]);
			out_scalar = mxGetPr(plhs[1]);

			//-- The output scalar is the number of distinct objects detected
			(*out_array1) = (double) (-1);
			(*out_scalar) = (double) (-1);
			#endif

			return;
		}

		//-- This is the Numerical Recipes Algorithm to resolve equivalence classes
		//-- Initialize each final class to itself
		for (k = 1; k <= n_labels; k++)
			nf[k] = k;
	
		//-- Loop through the designated equivalence classes
		for (i_eq_class = 1; i_eq_class <= n_eq_classes; i_eq_class++) {
	
			//-- Track first element up to its ancestor
			j = equivalence_classes_1[i_eq_class];
			while (nf[j] != j) j = nf[j];
		
			//-- Track second element up to its ancestor
			k = equivalence_classes_2[i_eq_class];
			while (nf[k] != k) k = nf[k];
		
			//-- If the first and second elements ancestors are
			//-- not already related, make them so
			if (j != k)
				nf[j] = k;
		
			//-- Make a final sweep up to highest ancestors
			for (j = 1; j <= n_labels; j++)
				while (nf[j] != nf[nf[j]])
					nf[j] = nf[nf[j]];
		}
	}

	
	//-- Here is the connected component labelling algorithm
	//-- second pass for resolving equivalence classes
	//-- Only need to enter the loop if the number of equivalence classes
	//-- is greater than zero.
	if (n_eq_classes > 0) {
		for (i_row = 0; i_row < n_rows; i_row++) {
		    for (j_col = 0; j_col < n_cols; j_col++) {

				//-- Skip pixels with index zero
				index = (i_row*n_cols)+j_col;
				if (connected_components[index] == 0)
					continue;
			
				//-- When a non-zero pixel is found, substitute the 
				//-- equivalence class value
				connected_components[index] = nf[connected_components[index]];
				
				//-- Count the number of pixels in each equivalence class
				nf_count[connected_components[index]]++;

		    }	//-- end for (j_col = ...
		}	//-- end for (i_row = ...
	}
	
	//-- Make a third pass here to eliminate equivalence classes that do not meet
	//-- the criteria for a star/celestial object
	n_valid_labels = 0;
	
	for (i_label = 1; i_label <= n_labels; i_label++) {
		
		//-- eliminate invalid labels, and collapse all valid labels
		if ((nf_count[i_label] >= min_star_pixels) && (nf_count[i_label] <= max_star_pixels)) {
			
			//-- the INDEX of the nf array will preserve the label number,
			//-- and the VALUES in the connected_components array will point
			//-- to the label number.
			n_valid_labels++;
			nf[i_label] = n_valid_labels;
		} else {
			//-- an invalid label number
			nf[i_label] = 0;
		}
	}
	
	//-- Once the labels have been screened and collapsed,
	//-- put the new labels into the connected_components array
	nf[0] = 0;
	for (i_row = 0; i_row < n_rows; i_row++) {
	    for (j_col = 0; j_col < n_cols; j_col++) {
			
			index = (i_row*n_cols)+j_col;
			connected_components[index] = nf[connected_components[index]];
		}
	}
	
//===================================================================================
//==
#ifdef DEBUG
	free (anomaly);
	
	//-- go through the motions as if being called from matlab
	dummy = (double) (n_valid_labels);

	//-- Store all detected objects into the output array
	for (i_row = 0; i_row < n_rows; i_row++)
	    for (j_col = 0; j_col < n_cols; j_col++)
			dummy = connected_components[(i_row*n_cols)+j_col];
#else

	//-- Allocate memory and assign output pointer
	plhs[0] = mxCreateDoubleMatrix (n_cols, n_rows, mxREAL);
	plhs[1] = mxCreateScalarDouble (0.0);

	//-- Get a pointer to the data space in the newly allocated memory
	out_array1 = mxGetPr(plhs[0]);
	out_scalar = mxGetPr(plhs[1]);

	//-- The output scalar is the number of distinct objects detected
	(*out_scalar) = (double) (n_valid_labels);

	//-- Store all detected objects into the output array
	for (i_row = 0; i_row < n_rows; i_row++)
	    for (j_col = 0; j_col < n_cols; j_col++)
			out_array1[(i_row*n_cols)+j_col] = connected_components[(i_row*n_cols)+j_col];
#endif
//==
//===================================================================================
	
	free (connected_components);
	free (equivalence_classes_1);
	free (equivalence_classes_2);
	free (nf);
	free (nf_count);

	return;
}
