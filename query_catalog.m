function expected_objects = query_catalog (ra_range, dec_range, station_info)

% The expected field of view is defined by limits in the two input vectors,
% ra_range and dec_range.  The range of right ascension will determine which
% USNO catalog files need to be read in.  Either one or two files will be
% required, depending on whether or not the expected field of view spans
% the RA range of more than one file.

% The ra_range vector could span 0 or 360 degrees (see estimate_fov.m comments).
% These will be special cases.

% The USNO catalog files contain 2x2-degree fields centered in declination
% over Broida Hall (34-24-50.8).  The file names indicate the range of RA
% values contained in the file, e.g., Broida_04h24m_04h32m.txt.
% These files were created using 8-minute RA ranges beginning at 00h00m RA.
% e.g.,
%     00h00m_00h_08m
%     00h08m_00h_16m
%     ...

% This routine needs to determine the correct file(s) to open, based on the input
% values for the range of RA, and the known file-naming scheme.

% The RA and Declination values are passed in as decimal-degree values (ddd.ddd)
% Calculate RA to the nearest hh-mm

% pull the beginning ra value into the range 0 to 360.  Based on the way ra_range
% was calculated in estimate_fov.m, the ra_range values will only be slightly
% beyond the 0 to 360 range.
if (ra_range(1) < 0)
    ra_low_degrees = ra_range(1) + 360;
    ra_high_degrees = ra_range(2) + 360;
else
    ra_low_degrees = ra_range(1);
    ra_high_degrees = ra_range(2);
end 

% Determine integer number of minutes for the starting RA
ra_start_mm = floor ((ra_low_degrees / 15) * 60);

% The file-naming scheme is modulo-8 in minutes RA, so trim the integer number
% of minutes to the next-lowest modulo-8 number of minutes
ra_start_minutes = ra_start_mm - mod (ra_start_mm, 8);

% Determine the start-hour in RA
ra_start_hours = floor (ra_start_minutes / 60);

% Determine the start-minutes past the hour in RA
ra_start_minutes = ra_start_minutes - (ra_start_hours * 60);

% The end-time of the file name is 8 minutes after the start
ra_end_minutes = ra_start_minutes + 8;
if (ra_end_minutes < 60)
    ra_end_hours = ra_start_hours;
else
    ra_end_minutes = ra_end_minutes - 60;
    ra_end_hours = ra_start_hours + 1;
    % note here that this addition is probably safe, i.e., the
    % file_end_hours will never be > 24.
    % The input value of ra_range(1) will never be 24h (360deg)
    % because of the way it is calculated in estimate_fov.m
    % IF the input RA in degrees is LESS THAN 360.0,
    % THEN the modulo-8 clipping will limit the number
    % of minutes RA start-time to a max of 23h52m
end

% Build the filename from the integer start/end hours and minutes
filename = sprintf ('Broida_Wide_%02dh%02dm_%02dh%02dm.txt', ...
    ra_start_hours, ra_start_minutes, ra_end_hours, ra_end_minutes);

% Read the file, which will return a matrix with RA, Dec, Magnitude and Catalog Number
message = sprintf ('            Catalog File: %s ...', filename);
disp (message);

filedir = cat (2, station_info.toss_base_dir, station_info.usno_catalog_files_dir);
filepath = fullfile (filedir, filename);
catalog_objects = csvread (filepath);
n = size (catalog_objects);
n_objects = n(1);

% Data from the USNO catalog will contain objects within a 2-degree by 2-degree 
% patch of sky. This list can be pared to an area that is larger 
% than the area of the input range by the station_info.fov_border parameter.
i_object = 1;
for i = 1:n_objects
    % Check criteria one at a time; if any criterion is not met, then
    % coninue to the next object in the list without storing the object.
    if ((catalog_objects(i,1) < ra_low_degrees) | (catalog_objects(i,1) > ra_high_degrees))
        continue; 
    elseif ((catalog_objects(i,2) < dec_range(1)) | (catalog_objects(i,2) > dec_range(2)))
        continue; 
    elseif (catalog_objects(i,3) > station_info.search_magnitude_cutoff) 
        continue; 
    end;
    
    % If all criteria have been met, then add info to the object struct
    expected_objects.ra(i_object) = catalog_objects(i,1);
    expected_objects.dec(i_object) = catalog_objects(i,2);
    expected_objects.mag(i_object) = catalog_objects(i,3);
    expected_objects.id1(i_object) = catalog_objects(i,4);
    expected_objects.id2(i_object) = catalog_objects(i,5);
    % Keep track of how many objects have actually been stored into
    % the expected_objects structure array.  The value of i_object
    % was initialized to 1 before the loop started.
    i_object = i_object + 1;
end

% Sometimes, an image frame will span the boundary of two USNO catalog files.
% In this case, the second USNO catalog file needs to be opened and searched.
% Does the image frame span two catalog files?  Use the calculated table end
% RA for comparison with the actual image ending RA.
ra_end_mm = (ra_high_degrees / 15.0) * 60.0;
ra_end_catalog_mm = (ra_end_hours * 60.0) + ra_end_minutes;

if (ra_end_mm <= ra_end_catalog_mm)
    return
end

%============================================================================
% If execution arrives here, then the image spans 2 catalog files,
% so get the second file.

if ((ra_range(1) < 0) | (ra_range(2) > 360))
    ra_start_hours = 0;
    ra_start_minutes = 0;
    ra_end_hours = 0;
    ra_end_minutes = 8;
else
    % Build the filename from the integer start/end hours and minutes
    ra_start_hours = ra_end_hours;
    ra_start_minutes = ra_end_minutes;

    % The end-time of the file name is 8 minutes after the start
    ra_end_minutes = ra_start_minutes + 8;
    if (ra_end_minutes < 60)
        ra_end_hours = ra_start_hours;
    else
        ra_end_minutes = ra_end_minutes - 60;
        ra_end_hours = ra_start_hours + 1;
    end
end

filename = sprintf ('Broida_Wide_%02dh%02dm_%02dh%02dm.txt', ...
    ra_start_hours, ra_start_minutes, ra_end_hours, ra_end_minutes);
filepath = fullfile (filedir, filename);

% Read the file, which will return a struct with Catalog Number, RA, Dec and Magnitude
message = sprintf ('            Catalog File: %s ...', filename);
disp (message);

catalog_objects = csvread (filepath);
n = size (catalog_objects);
n_objects = n(1);

% note: there will be a special case here when the
% image spans 24h RA.  The calling routine will send
% an RA value >24h; but, the correct file is
% Broida_00h00m_00h_08m.txt, and all the RA values
% in the file should have 24h added.
if (ra_range(2) > 360)
    ra_low_degrees = -1;
    ra_high_degrees = ra_range(2) - 360;
    ra_gt_24_flag = 360;
else
    ra_low_degrees = ra_range(1);
    ra_high_degrees = ra_range(2);
    ra_gt_24_flag = 0;
end 

% Data from the USNO catalog will contain objects within a 2-degree by 2-degree 
% patch of sky. This list can be pared to an area that is just slightly larger 
% than the area of the input range.
for i = 1:n_objects
    % Check criteria one at a time; if any criterion is not met, then
    % coninue to the next object in the list without storing the object.
    if ((catalog_objects(i,1) < ra_low_degrees) | (catalog_objects(i,1) > ra_high_degrees))
        continue; 
    elseif ((catalog_objects(i,2) < dec_range(1)) | (catalog_objects(i,2) > dec_range(2)))
        continue; 
    elseif (catalog_objects(i,3) > station_info.search_magnitude_cutoff) 
        continue; 
    end;
    
    % If all criteria have been met, then add info to the object struct
    expected_objects.ra(i_object) = catalog_objects(i,1) + ra_gt_24_flag;
    expected_objects.dec(i_object) = catalog_objects(i,2);
    expected_objects.mag(i_object) = catalog_objects(i,3);
    expected_objects.id1(i_object) = catalog_objects(i,4);
    expected_objects.id2(i_object) = catalog_objects(i,5);
    % Keep track of how many objects have actually been stored into
    % the expected_objects structure array.  The value of i_object
    % was initialized to 1 before the loop started.
    i_object = i_object + 1;
end
return