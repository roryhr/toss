function [dark_frame, bad_pixel_map] = read_dark_frame (station_info)

dark_frame_file_path = cat (2, station_info.toss_base_dir, station_info.dark_frame_file_path);
[dark_frame, dark_frame_info] = read_fits (dark_frame_file_path);

% The manual bad pixel map is stored as a text file on the disk.  Here, initialize
% the bad pixel map to the same size as the dark frame, and then upload the list
% of bad pixels into the new bad_pixel_map frame.
bad_pixel_map = dark_frame * 0;
bad_pixel_map = read_bad_pixel_list (bad_pixel_map, station_info);

% Significantly odd pixels in the dark frame are also deemed to be bad pixels.
% Run a median kernel filter around the frame to establish a low-spatial-frequency
% reference frame.  The median kernel output is unaffected by isolated bad pixels,
% so only the low spatial frequency components are passed through the filter.
dark_median_frame = median_x_dll (dark_frame, 15, 2);

% Subtracting the median frame from the dark frame removes low spatial frequency
% components, and produces an anomaly frame that ideally should be flat.  Any pixels
% that deviate significantly from the anomaly can be declared bad pixels.
anomaly_frame = dark_frame - dark_median_frame;

% Wherever there is a known bad pixel, set the anomaly to zero so these pixels
% will not skew the mean and variance calculations.
anomaly_frame(bad_pixel_map > 0.1) = 0;

% To find 'significantly odd' pixels, calculate the mean and variance of the
% anomaly frame.  Any pixels that lie more that 3 standard deviations from the
% the anomaly frame mean can be declared bad pixels.
anomaly_frame_mean = mean (anomaly_frame(:));
anomaly_frame_stdev = std (anomaly_frame(:));
lower_limit = anomaly_frame_mean - 3.0*anomaly_frame_stdev;
upper_limit = anomaly_frame_mean + 3.0*anomaly_frame_stdev;

% Update the bad pixel map wherever the anomaly frame is significantly deviant.
bad_pixel_map((anomaly_frame < lower_limit) | (anomaly_frame > upper_limit)) = 1;
