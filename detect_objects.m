function observed_objects = detect_objects (image_luminosity, bad_pixel_map, station_info, filename)

% In the Matlab Image Processing Toolbox, there are standard functions that might accomplish
% the basic goal of this module (The functions bwlabel and regionprops do the trick; see, e.g.,
% http://blogs.mathworks.com/steve/?p=62
% However, one goal for TOSS is to not rely on toolboxes here, so...

% Run a median-asterisk filter around the luminosity frame
median_frame = median_x_dll (image_luminosity, station_info.kernel_outer_radius, ...
    station_info.kernel_inner_radius);
median_frame = median_x_dll (median_frame, station_info.kernel_outer_radius, ...
    station_info.kernel_inner_radius);

% try to smooth it a little more with a median square filter
median_frame = median_square_dll (median_frame, 5);

anomaly = image_luminosity - median_frame;
clear ('median_frame');

% null the bad pixels
anomaly(bad_pixel_map > 0.1) = 0;

% identify peaks by standard deviation
anomaly_mean = mean (anomaly(:));
anomaly_stdev = std (anomaly(:));
background_limit = anomaly_mean + station_info.background_limit*anomaly_stdev;

anomaly(anomaly < background_limit) = 0;
anomaly(anomaly > .01) = 1;

% % identify peaks by 'fence outliers' (median-based)
% % sort each column individually
% sorted_anomaly = sort (anomaly);
% 
% % estimate the first and third quartiles from the sorted columns
% image_dimensions = size (image_luminosity);
% store = sorted_anomaly(floor(image_dimensions(1)*.25),:);
% first_quartile = median (store(:));
% store = sorted_anomaly(floor(image_dimensions(1)*.75),:);
% third_quartile = median (store(:));
% clear ('sorted_anomaly');
% clear ('store');
% iqr = third_quartile - first_quartile;
% fence_outlier = third_quartile + station_info.background_limit*iqr;
% 
% anomaly(anomaly < fence_outlier) = 0;
% anomaly(anomaly > .01) = 1;

%============================================================================================
% Debug:
% The binary anomaly can be written to a file, so that the connected_components_dll could 
% read the file for debugging that module
output_file_path = sprintf ('%s\\data\\debug_data\\%s_binary_anomaly.csv', station_info.toss_base_dir, filename);

image_dimensions = size (image_luminosity);
fid = fopen (output_file_path, 'w');
for i_row = 1:image_dimensions(1)
    for j_col = 1:image_dimensions(2)
        fprintf (fid, '%d\n', anomaly(i_row, j_col));
    end
end
fclose (fid);
%============================================================================================

% The DLL takes the anomaly frame and identifies isolated/connected objects within the frame
[connected_objects, n_objects] = connected_components_dll (anomaly, station_info.min_star_pixels, station_info.max_star_pixels);
if (n_objects == -1)
    observed_objects = -1;
    return
end

% Once connected objects have been identified, find the centroid of each object
observed_objects.centroid_x(1) = -1;
observed_objects.centroid_y(1) = -1;
observed_objects.estimated_mag(1) = -1;

% Check each object for validity, and find centroids for valid objects
for i_obj = 1:n_objects
    [row, col] = find (connected_objects == i_obj);
    n_pixels = size (row);
    
    % Could apply other criteria; but, if arrive here, the object
    % is valid according to the criteria, so find the centroid and intensity
    sum_x = 0;
    sum_y = 0;
    sum_i = 0;
    for i_pixel = 1:n_pixels
        val = image_luminosity(row(i_pixel), col(i_pixel));
        sum_x = sum_x + val*col(i_pixel);
        sum_y = sum_y + val*row(i_pixel);
        sum_i = sum_i + val;
    end
    
    % Store centroid and intensity information in the output variable observed_objects
    observed_objects.centroid_x(i_obj) = sum_x / sum_i;
    observed_objects.centroid_y(i_obj) = sum_y / sum_i;
    observed_objects.intensity(i_obj) = sum_i / station_info.exposure_time / n_pixels(1);
    % convert the observed object intensity into a magnitude based on empirical relationship
    observed_objects.estimated_mag(i_obj) = -5.446082*log10(observed_objects.intensity(i_obj) / station_info.exposure_time) + 24.544246;
end

%============================================================================================
% Debug: 
output_file_path = sprintf ('%s\\data\\debug_data\\%s_detected_objects.csv', station_info.toss_base_dir, filename);

fid = fopen (output_file_path, 'w');
for i = 1:n_objects
    fprintf (fid, '%d,%d,%d,%d\n', observed_objects.centroid_x(i), observed_objects.centroid_y(i), observed_objects.intensity(i), observed_objects.estimated_mag(i));
end
fclose (fid);
%============================================================================================