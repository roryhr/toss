function bad_pixel_map = read_bad_pixel_list (bad_pixel_map, station_info)

bad_pixel_list_file_path = cat (2, station_info.toss_base_dir, station_info.bad_pixel_list_file_path);
bad_pixel_list = csvread (bad_pixel_list_file_path);
n_bad_pixels = size (bad_pixel_list);

for i = 1:n_bad_pixels(1)
    bad_pixel_map(bad_pixel_list(i,1), bad_pixel_list(i,2)) = 1;
end