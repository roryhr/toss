#include "mex.h"
#define SEARCH_SIZE  9000

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int i,j, i_row, n_rows, j_col, n_cols, n_objects, search_radius, min_star_radius;
	int rim_sum_zero, rim_sum_nonzero;
	double exposure_time, max_pixel, center_min, sig_level, sum_x, sum_y, sum_i;
	double rim_sum_nonzero_percent, rim_sum_cutoff;

	double centroid_x[SEARCH_SIZE], centroid_y[SEARCH_SIZE], magnitude[SEARCH_SIZE];

//===================================================================================
	
	double *anomaly, *parameters, *out_array1, *out_array2, *out_array3;

	//-- Get input data array
	anomaly = mxGetPr (prhs[0]);
	n_rows = mxGetN (prhs[0]);
	n_cols = mxGetM (prhs[0]);
	
	//-- Parameters are passed in as an array
	parameters = mxGetPr (prhs[1]);
	search_radius = (int) (parameters[0]);
	min_star_radius = (int) (parameters[1]);
	exposure_time = parameters[2];
	rim_sum_cutoff = parameters[3];
	sig_level = parameters[4];

//===================================================================================
	
	n_objects = 0;
	centroid_x[0] = -1.0;
	centroid_y[0] = -1.0;
	magnitude[0] = -1.0;
	
	for (i_row = search_radius; i_row < n_rows - search_radius; i_row++) {
	    for (j_col = search_radius; j_col < n_cols - search_radius; j_col++) {
	    
	        // if the maximum pixel is right in the center of the roi, then the
	        // first criterion of object detection has been met
			max_pixel = anomaly[(i_row*n_cols)+j_col];
			for (i = i_row - search_radius; i <= i_row + search_radius; i++) {
				for (j = j_col - search_radius; j <= j_col + search_radius; j++) {
					if (anomaly[(i*n_cols)+j] > max_pixel) max_pixel = anomaly[(i*n_cols)+j];
				}
			}
	        
			if ((max_pixel > sig_level) && ((int) (max_pixel - anomaly[(i_row*n_cols)+j_col])) == 0) {
			
	            // if there are only non-zero pixels in the cells adjacent to the center max pixel,
	            // then the second criterion of object detection has been met
				center_min = anomaly[(i_row*n_cols)+j_col];
				for (i = i_row - min_star_radius; i <= i_row + min_star_radius; i++) {
					for (j = j_col - min_star_radius; j <= j_col + min_star_radius; j++) {
						if (anomaly[(i*n_cols)+j] < center_min) center_min = anomaly[(i*n_cols)+j];
					}
				}
	            
				if (center_min >= sig_level) {

	                // If the rim around the center is 'mostly' zero, then the third criterion of
	                // object detection has been met
					rim_sum_zero = 0;
					rim_sum_nonzero = 0;
					
					//-- Search in a square around the center pixel, first down the column edges
					for (i = i_row - search_radius; i <= i_row + search_radius; i++) {

						if (anomaly[(i*n_cols)+(j_col - search_radius)] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;

						if (anomaly[(i*n_cols)+(j_col + search_radius)] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;
					}

					//-- Then search across the row edges
					for (j = j_col - search_radius; j <= j_col + search_radius; j++) {

						if (anomaly[(i_row - search_radius)*n_cols+j] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;

						if (anomaly[(i_row + search_radius)*n_cols+j] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;
					}

					rim_sum_nonzero_percent = 100.0 * (double) rim_sum_nonzero / (double) (rim_sum_nonzero + rim_sum_zero);

	                if (rim_sum_nonzero_percent <= rim_sum_cutoff) {

	                    // Calculate Centroid
	                    sum_x = 0.0;
	                    sum_y = 0.0;
	                    sum_i = 0.0;
	                    for (i = i_row - search_radius; i <= i_row + search_radius; i++) {
	                        for (j = j_col - search_radius; j <= j_col + search_radius; j++) {
	                            if (anomaly[(i*n_cols)+j] > 0) {
	                                sum_x += (double)(i)*anomaly[(i*n_cols)+j];
	                                sum_y += (double)(j)*anomaly[(i*n_cols)+j];
	                                sum_i += anomaly[(i*n_cols)+j];
	                            }
	                        }
	                    }
                    
	                    centroid_x[n_objects] = sum_x / sum_i;
	                    centroid_y[n_objects] = sum_y / sum_i;
	                    magnitude[n_objects] = sum_i / exposure_time;
	                    n_objects += 1;
                    
	                    if (n_objects >= SEARCH_SIZE) goto exit_loop;
	                }
	            }
	        }
	    }
	}
	
exit_loop:

	if (n_objects == 0) n_objects = 1;

//===================================================================================
	
	//-- Allocate memory and assign output pointer
	plhs[0] = mxCreateDoubleMatrix (1, n_objects, mxREAL);
	plhs[1] = mxCreateDoubleMatrix (1, n_objects, mxREAL);
	plhs[2] = mxCreateDoubleMatrix (1, n_objects, mxREAL);

	//-- Get a pointer to the data space in our newly allocated memory
	out_array1 = mxGetPr(plhs[0]);
	out_array2 = mxGetPr(plhs[1]);
	out_array3 = mxGetPr(plhs[2]);

	//-- Store all detected objects into the output arrays
	for (i = 0; i < n_objects; i++) {
		out_array1[i] = centroid_x[i];
		out_array2[i] = centroid_y[i];
		out_array3[i] = magnitude[i];
	}

	return;
}
/*
void main()
{
	int i,j, i_row, n_rows, j_col, n_cols, n_objects, search_radius, min_star_radius;
	int rim_sum_zero, rim_sum_nonzero;
	double exposure_time, max_pixel, center_min, sig_level, sum_x, sum_y, sum_i;
	double rim_sum_nonzero_percent, rim_sum_cutoff;

//===================================================================================
	
	double anomaly[25000], centroid_x[SEARCH_SIZE], centroid_y[SEARCH_SIZE], magnitude[SEARCH_SIZE], parameters[5];
	
	n_rows = 100;
	n_cols = 250;
	
	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {
			if (((i_row >= 20) && (i_row <= 25)) && ((j_col >= 30) && (j_col <= 35))) {
				anomaly[(i_row*n_cols)+j_col] = (double) (i_row + j_col);
			} else {
				anomaly[(i_row*n_cols)+j_col] = 0.0;
			}
			if ((i_row == 22) && (j_col == 33)) anomaly[(i_row*n_cols)+j_col] = 1000.0;
		}
	}

	search_radius = (int) (parameters[0] = 15.0);
	min_star_radius = (int) (parameters[1] = 1.0);
	exposure_time = (parameters[2] = 30.0);
	rim_sum_cutoff = (parameters[3] = 0.0);
	sig_level = (parameters[4] = 10.0);

//===================================================================================
	
	n_objects = 0;
	centroid_x[0] = -1.0;
	centroid_y[0] = -1.0;
	magnitude[0] = -1.0;
	
	for (i_row = search_radius; i_row < n_rows - search_radius; i_row++) {
	    for (j_col = search_radius; j_col < n_cols - search_radius; j_col++) {
	    
	        // if the maximum pixel is right in the center of the roi, then the
	        // first criterion of object detection has been met
			max_pixel = anomaly[(i_row*n_cols)+j_col];
			for (i = i_row - search_radius; i <= i_row + search_radius; i++) {
				for (j = j_col - search_radius; j <= j_col + search_radius; j++) {
					if (anomaly[(i*n_cols)+j] > max_pixel) max_pixel = anomaly[(i*n_cols)+j];
				}
			}
	        
			if ((max_pixel > sig_level) && ((int) (max_pixel - anomaly[(i_row*n_cols)+j_col])) == 0) {
			
	            // if there are only non-zero pixels in the cells adjacent to the center max pixel,
	            // then the second criterion of object detection has been met
				center_min = anomaly[(i_row*n_cols)+j_col];
				for (i = i_row - min_star_radius; i <= i_row + min_star_radius; i++) {
					for (j = j_col - min_star_radius; j <= j_col + min_star_radius; j++) {
						if (anomaly[(i*n_cols)+j] < center_min) center_min = anomaly[(i*n_cols)+j];
					}
				}
	            
				if (center_min >= sig_level) {

	                // If the rim around the center is 'mostly' zero, then the third criterion of
	                // object detection has been met
					rim_sum_zero = 0;
					rim_sum_nonzero = 0;
					
					//-- Search in a square around the center pixel, first down the column edges
					for (i = i_row - search_radius; i <= i_row + search_radius; i++) {

						if (anomaly[(i*n_cols)+(j_col - search_radius)] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;

						if (anomaly[(i*n_cols)+(j_col + search_radius)] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;
					}

					//-- Then search across the row edges
					for (j = j_col - search_radius; j <= j_col + search_radius; j++) {

						if (anomaly[(i_row - search_radius)*n_cols+j] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;

						if (anomaly[(i_row + search_radius)*n_cols+j] < 0.1)
							rim_sum_zero++;
						else
							rim_sum_nonzero++;
					}

					rim_sum_nonzero_percent = 100.0 * (double) rim_sum_nonzero / (double) (rim_sum_nonzero + rim_sum_zero);

	                if (rim_sum_nonzero_percent <= rim_sum_cutoff) {

	                    // Calculate Centroid
	                    sum_x = 0.0;
	                    sum_y = 0.0;
	                    sum_i = 0.0;
	                    for (i = i_row - search_radius; i <= i_row + search_radius; i++) {
	                        for (j = j_col - search_radius; j <= j_col + search_radius; j++) {
	                            if (anomaly[(i*n_cols)+j] > 0) {
	                                sum_x += (double)(i)*anomaly[(i*n_cols)+j];
	                                sum_y += (double)(j)*anomaly[(i*n_cols)+j];
	                                sum_i += anomaly[(i*n_cols)+j];
	                            }
	                        }
	                    }
                    
	                    centroid_x[n_objects] = sum_x / sum_i;
	                    centroid_y[n_objects] = sum_y / sum_i;
	                    magnitude[n_objects] = sum_i / exposure_time;
	                    n_objects += 1;
                    
	                    if (n_objects >= SEARCH_SIZE) goto exit_loop;
	                }
	            }
	        }
	    }
	}
	
exit_loop:

	if (n_objects == 0) n_objects = 1;

//===================================================================================
	
	return;
}
*/
