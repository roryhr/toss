function lst_deg = LST(y, m, d, ut_hr, ut_min, ut_sec, west_lon)
% This function calculates the local sidereal time for a given
% date and UT at a given longitude
% lst_deg - local sidereal time (degrees) 
% y - year 
% m - month 
% d - day 
% ut_hr - Universal Time (hours) 
% ut_min - Universal Time (minutes) 
% ut_sec - Universal Time (seconds) 
% west_lon - west longitude (degrees) 
% j0 - Julian day number at hr UT 
% j - number of centuries since J2000 
% g0 - Greenwich sidereal time (degrees) at  hr UT 
% gst - Greenwich sidereal time (degrees) at the specified UT 
% User M-function required : JO 
%------------------------------------------------------------

% % test algorithm using the example from the book p. 625
% % East Longitude
% degrees = 139;
% minutes = 47;
% seconds = 0;
% west_lon = 360 - (degrees + minutes/60 + seconds/3600);
% % Date
% y = 2004;
% m = 3;
% d = 3;
% % Universal Time
% ut_hr = 4;
% ut_min = 30;
% ut_sec = 0;

% Castor Star Field for file 02_13_4412674.crw from 3/25/2008
% y = 2008;
% m = 3;
% d = 3;
% ut_hr = 2 + 4;
% ut_min = 13;
% ut_sec = 44.12674;
% west_lon = 360 - (119 + 50/60 + 33.2/3600);

% ... Equation 5.48:
j0 = J0(y, m, d);

% ... Equation 5.49: 
j = (j0 - 2451545)/36525;

% ... Equation 5.50: 
gO = 100.4606184 + 36000.77004*j + .000387933*j^2 - 2.583e-8*j^3;

% ... Reduce gO so it lies in the range 0 - 360 degrees 
gO = zeroTo360 (gO); 

% ... Equation 5.51:
ut = ut_hr + ut_min/60 + ut_sec/3600;
gst = gO + 360.98564724*ut/24;

% ... Equation 5.52: 
east_lon = 360 - west_lon;
lst_deg = gst + east_lon;

% ... Reduce lst to the range 0 - 360 degrees:
lst_deg = lst_deg - 360*fix(lst_deg/360);

return 

%------------------------------------------------------------

function y = zeroTo360(x)
%------------------------- 
% This subfunction reduces an angle to the range  - 360 degrees 
% x - The angle (degrees) to be reduced 
% y - The reduced value 
if ( x >= 360 ) 
    x = x - fix (x/360)*360; 
elseif ( x < 0) 
    x = x - (fix (x/360) - 1)*360; 
end 

y = x;

return 

% COPYRIGHTED MATERIAL
% Howard Curtis. Orbital Mechanics: For Engineering Students 
% (Aerospace Engineering). (Butterworth-Heinemann, 2004). Page BM43.