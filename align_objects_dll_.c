#include <math.h>
#include "mex.h"
#define BIG_NUMBER  1.0E99

//#define DEBUG

#ifdef DEBUG
	#define N_OBSERVED 20
	#define N_EXPECTED 1539

	void main()
	{
#else
	void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
	{
#endif

	int i_obj, j_obj, n_observed, n_expected, i_base_obs_obj, i_shift, min_index;
	int global_min_nn_exp_index, global_min_nn_obs_index;
	int global_min_total_nn_distances_obs_index, min_total_nn_distances_exp_index;
	int *nearest_neighbor_index;
	double mid_observed_ra, mid_observed_dec, dist_to_mid, dist_to_mid_min;
	double min_total_nn_distances, global_min_total_nn_distances;
	double expected_ra_min, expected_ra_max, expected_dec_min, expected_dec_max;
	double observed_ra_min, observed_ra_max, observed_dec_min, observed_dec_max;
	double shift_right_max, shift_left_max, shift_up_max, shift_down_max, dist;
	double shift_ra, shift_dec, min_nn_dist, sum_nn_distances, min_nn_dist_total;
	float *store_observed_ra, *store_observed_dec, *nn_dist_total;

//===================================================================================
//==
#ifdef DEBUG
	float observed_ra[N_OBSERVED], observed_dec[N_OBSERVED], observed_intensity[N_OBSERVED];
	float observed_magnitude[N_OBSERVED];
	float expected_ra[N_EXPECTED], expected_dec[N_EXPECTED], expected_magnitude[N_EXPECTED];
	char line_in[256];
	char base_path[256];
	char path[512];
	FILE *fp_in, *fp_out;

	n_observed = N_OBSERVED;
	n_expected = N_EXPECTED;

	//-- Read in an actual data set from a TOSS image
	sprintf (base_path,"C:\\1work\\Misc\\TOSS\\release\\toss.programs\\data\\debug_data\\05_23_5910334.fit");
	sprintf (path, "%s_observed_objects.csv", base_path);
	fp_in = fopen (path,"r");
	for (i_obj = 0; i_obj < n_observed; i_obj++)
		fscanf (fp_in, "%f,%f,%f", &observed_ra[i_obj],&observed_dec[i_obj],&observed_intensity[i_obj]);
	fclose (fp_in);

	sprintf (path, "%s_expected_objects.csv", base_path);
	fp_in = fopen (path,"r");
	for (i_obj = 0; i_obj < n_expected; i_obj++)
		fscanf (fp_in, "%f,%f,%f", &expected_ra[i_obj],&expected_dec[i_obj],&expected_magnitude[i_obj]);
	fclose (fp_in);
	
#else
	double *observed_ra, *observed_dec, *expected_ra, *expected_dec;
	double *shift_ra_optimal, *shift_dec_optimal;

	//-- Get input data arrays
	observed_ra = mxGetPr (prhs[0]);
	observed_dec = mxGetPr (prhs[1]);
	n_observed = mxGetN (prhs[0]);
	expected_ra = mxGetPr (prhs[2]);
	expected_dec = mxGetPr (prhs[3]);
	n_expected = mxGetN (prhs[2]);

#endif
//==
//===================================================================================
	
//-- The two patterns should be able to line up when one pattern is laid on top of the other.
//-- When the patterns are eventually aligned, all of the objects in the observed_objects list 
//-- will (hopefully) be situated very close to one of the objects in the expected_objects
//-- list.  
//-- 

//-- Allocate temporary space for the shifted-objects arrays
store_observed_ra = (float *)malloc((size_t) (n_observed*sizeof(float)));
store_observed_dec = (float *)malloc((size_t) (n_observed*sizeof(float)));
nn_dist_total = (float *)malloc((size_t) (n_expected*sizeof(float)));
nearest_neighbor_index = (int *)malloc((size_t) (n_observed*sizeof(int)));
min_nn_dist_total = BIG_NUMBER;

//-- Want to limit the amount of allowable shift when trying to align the observed field to
//-- the expected field.  A simple limit would be to not let the observed field wander outside
//-- of the expected field.  Here, calculate shift limits:
expected_ra_min = expected_ra[0];
expected_ra_max = expected_ra[0];
expected_dec_min = expected_dec[0];
expected_dec_max = expected_dec[0];
for (i_obj = 1; i_obj < n_expected; i_obj++) {
	if (expected_ra[i_obj] < expected_ra_min)
		expected_ra_min = expected_ra[i_obj];
	if (expected_ra[i_obj] > expected_ra_max)
		expected_ra_max = expected_ra[i_obj];
	if (expected_dec[i_obj] < expected_dec_min)
		expected_dec_min = expected_dec[i_obj];
	if (expected_dec[i_obj] > expected_dec_max)
		expected_dec_max = expected_dec[i_obj];
}
observed_ra_min = observed_ra[0];
observed_ra_max = observed_ra[0];
observed_dec_min = observed_dec[0];
observed_dec_max = observed_dec[0];
for (i_obj = 1; i_obj < n_observed; i_obj++) {
	if (observed_ra[i_obj] < observed_ra_min)
		observed_ra_min = observed_ra[i_obj];
	if (observed_ra[i_obj] > observed_ra_max)
		observed_ra_max = observed_ra[i_obj];
	if (observed_dec[i_obj] < observed_dec_min)
		observed_dec_min = observed_dec[i_obj];
	if (observed_dec[i_obj] > observed_dec_max)
		observed_dec_max = observed_dec[i_obj];
}

//-- put some reasonable limits on the maximum allowable shift
shift_right_max = expected_ra_max - observed_ra_max;
if (shift_right_max <= 0.0) shift_right_max = 0.1;

shift_left_max = expected_ra_min - observed_ra_min;
if (shift_left_max >= 0.0) shift_left_max = -0.1;

shift_up_max = expected_dec_max - observed_dec_max;
if (shift_up_max <= 0.0) shift_up_max = 0.1;

shift_down_max = expected_dec_min - observed_dec_min;
if (shift_down_max >= 0.0) shift_down_max = -0.1;

//-- For each of the observed objects , shift the observed pattern so that the baseline observed
//-- object is aligned with one of the expected objects.
//-- In a shifted configuration, find the expected object that is closest to each observed
//-- object.  The metric for the shifted configuration is the sum of all nearest-neighbor
//-- distances.
//-- The best (translation-only) alignment occurs at the smallest nearest-neighbor distance.
//-- This alignment is determined by recording the baseline observed object index and the 
//-- expected object index where the global minimum occurs.
global_min_total_nn_distances = BIG_NUMBER;
global_min_total_nn_distances_obs_index = 0;

for (i_base_obs_obj = 0; i_base_obs_obj < n_observed; i_base_obs_obj++) {
	
	min_total_nn_distances = BIG_NUMBER;
	min_total_nn_distances_exp_index = 0;

	//-- Fore a given baseline observed object, calculate the sum
	//-- of all nearest neighbor distances.  The sum of nearest-neighbor
	//-- distances is calcluated for each alignment shift, where the observed pattern is
	//-- shifted so that the baseline observed object is placed one-at-
	//-- a-time over each of the expected template
	for (i_shift = 0; i_shift < n_expected; i_shift++) {

		//-- Calculate the shift between the observed base object and the next object
		//-- in the expected objects list
		shift_ra = expected_ra[i_shift] - observed_ra[i_base_obs_obj];
		shift_dec = expected_dec[i_shift] - observed_dec[i_base_obs_obj];
		
		//-- If the calculated shift moves the observed field outside the expected
		//-- field of view, then there is no need to check the current shifted alignment
		if ((shift_ra < shift_left_max) || (shift_ra > shift_right_max)) {
			nn_dist_total[i_shift] = BIG_NUMBER;
			continue;
		}
		if ((shift_dec < shift_down_max) || (shift_dec > shift_up_max)) {
			nn_dist_total[i_shift] = BIG_NUMBER;
			continue;
		}

		//-- Apply the shift to the observed objects list, and store the shifted objectes
		//-- in temporary arrays
		for (i_obj = 0; i_obj < n_observed; i_obj++) {
			store_observed_ra[i_obj] = observed_ra[i_obj] + shift_ra;
			store_observed_dec[i_obj] = observed_dec[i_obj] + shift_dec;
		}

		//-- For each (shifted) observed object, calculate the distance to every expected object.
		//-- The minimum distance in the list will become the nearest-neighbor distance for
		//-- the (shifted) observed object.
		sum_nn_distances = 0.0;
		for (i_obj = 0; i_obj < n_observed; i_obj++) {
			
			min_nn_dist = BIG_NUMBER;
		
			//-- Calculate the distance from each expected object to the current observed object
		    for (j_obj = 0; j_obj < n_expected; j_obj++) {
				
		        dist = sqrt ((store_observed_ra[i_obj] - expected_ra[j_obj]) *
		        			 (store_observed_ra[i_obj] - expected_ra[j_obj])
		                   + (store_observed_dec[i_obj] - expected_dec[j_obj]) *
		                     (store_observed_dec[i_obj] - expected_dec[j_obj]));
				
				//-- Determine the expected object that is the closest to 
				//-- the current observed object
				if (dist < min_nn_dist)
					min_nn_dist = dist;
				if (min_nn_dist < 1.0e-10)
					break;
		    }
		    
		    //-- Try to prevent two observed objects from having the same nearest
		    //-- neighbor in the expected objects list
		    //-- **** NOTE:  Check logic here ****
		    //for (j_obj = 0; j_obj < i_obj - 1; j_obj++)
		        //dist[nn_dist_index[j_obj]] = BIG_NUMBER;

			//-- Accumulate the sum of minimum distances for the current alignment shift
			sum_nn_distances += min_nn_dist;

		}
		//-- If the total sum of nearest-neighbor distances for the current alignment shift
		//-- is smaller than any previous sum, then replace the sum
		if (sum_nn_distances < min_total_nn_distances) {
			min_total_nn_distances = sum_nn_distances;
			min_total_nn_distances_exp_index = i_shift;
		}
	}
	
	//-- Determine the shift that yielded the smallest total sum of minimum distances,
	//-- as this should indicate the best translation-only alignment of observed
	//-- objects to expected objects when placing the baseline observed object above one
	//-- of the expected objects.
	//-- Determine the shift that yielded the smallest total sum of minimum distances
	//-- among the list of smallest shifts for each baseline object
	if (min_nn_dist < min_nn_dist_total) {
		min_nn_dist_total = min_nn_dist;
		global_min_nn_exp_index = min_total_nn_distances_exp_index;
		global_min_nn_obs_index = i_base_obs_obj;
	}
}

//-- Calculate the shift in RA and Dec corresponding to the global minimum nearest-neighbor
//-- total distance, and apply the shifts to the original observed object locations.
shift_ra = expected_ra[global_min_nn_exp_index] - observed_ra[global_min_nn_obs_index];
shift_dec = expected_dec[global_min_nn_exp_index] - observed_dec[global_min_nn_obs_index];

min_total_nn_distances_exp_index = 0;

#ifdef DEBUG
	sprintf (path, "%s_objects_shift.txt", base_path);
	fp_out = fopen (path,"w");
	fprintf (fp_out, "%f,%f", shift_ra, shift_dec);
	fclose (fp_out);
#endif
	
for (i_obj = 0; i_obj < n_observed; i_obj++) {
	store_observed_ra[i_obj] = observed_ra[i_obj] + shift_ra;
	store_observed_dec[i_obj] = observed_dec[i_obj] + shift_dec;
}

//-- For each (shifted) observed object, find the nearest neighbor in the expected objects list
for (i_obj = 0; i_obj < n_observed; i_obj++) {
	
	min_nn_dist = BIG_NUMBER;

	//-- Calculate the distance from each expected object to the current observed object
    for (j_obj = 0; j_obj < n_expected; j_obj++) {
		
        dist = sqrt ((store_observed_ra[i_obj] - expected_ra[j_obj]) *
        			 (store_observed_ra[i_obj] - expected_ra[j_obj])
                   + (store_observed_dec[i_obj] - expected_dec[j_obj]) *
                     (store_observed_dec[i_obj] - expected_dec[j_obj]));
		
		//-- Determine the expected object that is the closest to 
		//-- the current observed object
		if (dist < min_nn_dist) {
			min_nn_dist = dist;
			nearest_neighbor_index[i_obj] = j_obj;
		}
    }
}

#ifdef DEBUG
	sprintf (path, "%s_nearest_neighbors.csv", base_path);
	fp_out = fopen (path,"w");
	for (i_obj = 0; i_obj < n_observed; i_obj++)
		fprintf (fp_out, "%f,%f,%f,%f,%f,%f\n", store_observed_ra[i_obj],
												store_observed_dec[i_obj],
												observed_intensity[i_obj],
									            expected_ra[nearest_neighbor_index[i_obj]],
									            expected_dec[nearest_neighbor_index[i_obj]],
									            expected_magnitude[nearest_neighbor_index[i_obj]]);
	fclose (fp_out);
#endif

	free (store_observed_ra);
	free (store_observed_dec);
	free (nn_dist_total);
	free (nearest_neighbor_index);

//===================================================================================
//==
#ifndef DEBUG

	//-- Allocate memory and assign output pointer
	plhs[0] = mxCreateScalarDouble (0.0);
	plhs[1] = mxCreateScalarDouble (0.0);

	//-- Get a pointer to the data space in the newly allocated memory
	shift_ra_optimal = mxGetPr(plhs[0]);
	shift_dec_optimal = mxGetPr(plhs[1]);

	//-- The output scalars are the optimal values of ra and dec shift
	(*shift_ra_optimal) = (double) (shift_ra);
	(*shift_dec_optimal) = (double) (shift_dec);

#endif
//==
//===================================================================================
	
	return;
}
