function [flat_field, bad_pixel_map] = read_flat_field (station_info, bad_pixel_map)

flat_field_file_path = cat (2, station_info.toss_base_dir, station_info.flat_field_file_path);
[flat_field, flat_field_info] = read_fits (flat_field_file_path);

% Significantly odd pixels in the flat field are also deemed to be bad pixels.
% Run a median kernel filter around the frame to establish a low-spatial-frequency
% reference frame.  The median kernel output is unaffected by isolated bad pixels,
% so only the low spatial frequency components are passed through the filter.
median_frame = median_x_dll (flat_field, 15, 2);

% where there are known bad pixels already, set these pixels to median frame value
flat_field(bad_pixel_map > 0) = median_frame(bad_pixel_map > 0);

% Subtracting the median frame from the flat field removes low spatial frequency
% components, and produces an anomaly frame that ideally should be flat.  Any pixels
% that deviate significantly from the anomaly can be declared bad pixels.
anomaly = flat_field - median_frame;

% To find 'significantly odd' pixels, calculate the mean and variance of the
% anomaly frame.  Any pixels that lie more that ~3 standard deviations from the
% the anomaly frame mean can be declared bad pixels.
anomaly_mean = mean (anomaly(:));
anomaly_stdev = std (anomaly(:));
lower_limit = anomaly_mean - 3.0*anomaly_stdev;
upper_limit = anomaly_mean + 3.0*anomaly_stdev;

% Update the bad pixel map wherever the anomaly frame is significantly deviant.
anomaly((anomaly > lower_limit) & (anomaly < upper_limit)) = 0;
anomaly(anomaly ~= 0) = 1;

bad_pixel_map = bad_pixel_map + anomaly;

% Update the flat field map to mask out known bad pixels
flat_field(bad_pixel_map > .1) = median_frame(bad_pixel_map > .1);

% This step creates a 'gain correction' from the flat field map
flat_field_max = max (flat_field(:));
flat_field = flat_field / flat_field_max;
flat_field(flat_field == 0) = 1;