
function station_info = read_station_info (station_file_path)
% This function reads station info from the data file located at
% the file path contained in the input string 'station_file_path'.
% This returns the data in a struct called 'station_info'.

% The input file is assumed to be a comma-separated values file (.csv)

% This matlab script must be compatible with the format of the
% input file; any changes to the structure of the input file will
% force changes to to be made to this script.  Otherwise, the input
% data will be read into the wrong variables in the struct, and the
% rest of the program will not function properly.

% The basic structure of the input file includes one parameter per
% line, in the format
%     label,value

% Since the station file includes both numeric and string data
% in the value column, the easiest way to read this in is by using
% Matlab's textread command;  everything will come in as strings,
% and the numeric values are converted from the strings.
% The one line reads the entire data file into the two arrays.
[labels,values] = textread (station_file_path,'%s %s','delimiter',',');

% Here is where the format of the input file is critical: 
% must store each value from the file into the appropriate 
% struct location, which is hard-coded in the following lines.
% All the i_line = i_line + 1 lines may look funny, but doing it this way makes
% it a whole lot easier to add a parameter in the middle of the list.
i_line = 1;
station_info.toss_image_version = char (values(i_line));

i_line = i_line + 1;
station_lat_dd = str2double (values(i_line));

i_line = i_line + 1;
station_lat_mm = str2double (values(i_line));

i_line = i_line + 1;
station_lat_ss = str2double (values(i_line));

station_info.station_lat = station_lat_dd + station_lat_mm/60.0 + station_lat_ss/3600.0;

i_line = i_line + 1;
station_lon_dd = str2double (values(i_line));

i_line = i_line + 1;
station_lon_mm = str2double (values(i_line));

i_line = i_line + 1;
station_lon_ss = str2double (values(i_line));

station_info.station_lon = station_lon_dd + station_lon_mm/60.0 + station_lon_ss/3600.0;

i_line = i_line + 1;
station_info.station_elev = str2double(values(i_line));

i_line = i_line + 1;
station_info.toss_base_dir = char (values(i_line));

i_line = i_line + 1;
station_info.images_to_process_dir = char (values(i_line));

i_line = i_line + 1;
station_info.dark_frame_file_path = char (values(i_line));

i_line = i_line + 1;
station_info.flat_field_file_path = char (values(i_line));

i_line = i_line + 1;
station_info.usno_catalog_files_dir = char (values(i_line));

i_line = i_line + 1;
station_info.bad_pixel_list_file_path = char (values(i_line));

i_line = i_line + 1;
station_info.rawconvert_batch_file_path = char (values(i_line));

% Parameters used in 
i_line = i_line + 1;
station_info.search_magnitude_cutoff = str2double (values(i_line));

i_line = i_line + 1;
station_info.kernel_outer_radius = str2double (values(i_line));

i_line = i_line + 1;
station_info.kernel_inner_radius = str2double (values(i_line));

% Parameters used in detect_objects.m
i_line = i_line + 1;
station_info.background_limit = str2double (values(i_line));

i_line = i_line + 1;
station_info.max_star_pixels = str2double (values(i_line));

i_line = i_line + 1;
station_info.min_star_pixels = str2double (values(i_line));

i_line = i_line + 1;
station_info.exposure_time = str2double (values(i_line));

% Parameters used in align_objects.m
i_line = i_line + 1;
station_info.pixel_pitch_x = str2double (values(i_line));

i_line = i_line + 1;
station_info.pixel_pitch_y = str2double (values(i_line));

i_line = i_line + 1;
station_info.n_pixels_x = str2double (values(i_line));

i_line = i_line + 1;
station_info.n_pixels_y = str2double (values(i_line));

i_line = i_line + 1;
station_info.fov_border_pixels = str2double (values(i_line));

i_line = i_line + 1;
station_info.nn_outlier_cutoff = str2double (values(i_line));

i_line = i_line + 1;
station_info.nn_association_cutoff = str2double (values(i_line));