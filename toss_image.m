% TOSS Image Processing Routine.
%================================

% toss_image.m

% This is the main program called from the Matlab command prompt to process image
% files in the input directory.

% ************   IMPORTANT   **************
% The Matlab "Current Directory" must be set to the directory containing this file, 
% which must be the ...\toss.programs\Source directory

% ************   IMPORTANT   **************
% All path information required to run the program is contained in the station
% info file, toss_station_data.csv.  This file is in the same directory as toss_image.m
% The toss_station_data.csv MUST BE EDITED prior to running toss_image.m
% The TOSS base directory (toss_base_dir) must be entered into toss_station_data.csv
% This parameter identifies the base location of the TOSS Matlab files

% The directory structure expected by this program is:
%   [toss_base_dir]
%       \data
%            \images_to_process
%               \YYYY_MM_DD (example: 2007_11_10)
%                  \[all .fit files in the date sub-directories will be processed]
%            \star_catalog
%               \[180 2deg RA x 5deg Dec .csv files from the USNO B1.0 database]
%            \station_data
%               [toss dark frame files for each camera]
%               [toss flat-field files for each camera]
%               [manually declared bad pixel list]
%       \documentation
%       \log
%       \source
%            [toss_station_data.csv]
%            [all Matlab .m source files and .dll mex files required for TOSS]

% This program can be Executed from the Matlab command window:
% >> toss_image

% Call function to read in the station info file
station_info = read_station_info ('toss_station_data.csv');

% Update the Matlab Command Window with some status information
message = sprintf ('TOSS Image Processing Routine ver. %s ...', station_info.toss_image_version);
disp (message);

% Call functions to read in the dark-frame and flat-field files
disp ('   Reading Dark Frame and Finding Bad Pixels ...');
[dark_frame, bad_pixel_map] = read_dark_frame (station_info);
disp ('   Reading Flat Field and Adding to the Bad-Pixels Map ...');
[flat_field, bad_pixel_map] = read_flat_field (station_info, bad_pixel_map);

% Get a directory listing of all the date subdirectories to process in 
% the image base directory
images_to_process_dir = cat (2, station_info.toss_base_dir, station_info.images_to_process_dir);
input_image_dir = sprintf ('%s*', images_to_process_dir);

% Currently, the individual .FIT files are in directories for each date
list_dates = dir (input_image_dir);
n_dates = size (list_dates);
for i_date = 1:n_dates(1)
    if (strcmp(list_dates(i_date).name, '.') == 1)
        continue;
    end
    if (strcmp(list_dates(i_date).name, '..') == 1)
        continue;
    end
    if (~list_dates(i_date).isdir) 
        continue;
    end
    
    year = str2double (list_dates(i_date).name(1:4));
    month = str2double (list_dates(i_date).name(6:7));
    day = str2double (list_dates(i_date).name(9:10));

    % get the list of .FIT files in each date directory
    fit_files_path = sprintf ('%s%s\\*.fit', images_to_process_dir, list_dates(i_date).name);
    list_fit_files = dir (fit_files_path);
    n_fit_files = size (list_fit_files);
    message = sprintf ('   Number of files to process: %d', n_fit_files(1));
    disp (message);
            
    % loop through the fit files
    for i_fit_file = 1:n_fit_files(1)
        fit_file_path = sprintf ('%s%s\\%s', images_to_process_dir, list_dates(i_date).name, list_fit_files(i_fit_file).name);
        message = sprintf ('      Processing file %s\\%s:', list_dates(i_date).name, list_fit_files(i_fit_file).name);
        disp (message);
        disp ('      --> Reading Image File ...');
        [image_luminosity, fits_header] = read_fits (fit_file_path);

        % Image Calibration/Non-Uniformity Correction
        disp ('      --> Calibrating Image Data ...');
        image_luminosity = calibrate_image (image_luminosity, dark_frame, flat_field, bad_pixel_map);

        % Send the calibrated image frame to the object-detect routine
        disp ('      --> Detecting Objects in Calibrated Frame ...');
        observed_objects = detect_objects (image_luminosity, bad_pixel_map, station_info, list_fit_files(i_fit_file).name);

        % Determine the Image's Estimated Field of View, and Read the Star Catalog
        disp ('      --> Reading the USNO B1.0 Star Catalog ...');
        hour = str2double (list_fit_files(i_fit_file).name(1:2));
        minute = str2double (list_fit_files(i_fit_file).name(4:5));
        seconds = 0.00001 * str2double (list_fit_files(i_fit_file).name(7:13));
        [ra_range, dec_range] = estimate_fov (year, month, day, hour, minute, seconds, station_info);
        expected_objects = query_catalog (ra_range, dec_range, station_info);
    
        % Align the pattern of observed objects to the patter of expected objects
        disp ('      --> Aligning Observed Objects to Expected Objects ...');
        aligned_objects = align_objects (observed_objects, expected_objects, ra_range, dec_range, station_info, list_fit_files(i_fit_file).name);    
    end
end