function [image_luminosity, fits_header] = read_fits(file_path)
% This function reads a .FITS file from the input file_path.
% Data in the file are arranged in 3 frames that represent
% r,g,b intensity.  For TOSS, the initial outlook is to extract
% luminance information only; chrominance may be explored at a
% later time.

% Any information in the .FITS header will be read in to a struct
fits_header = fitsinfo (file_path);

% This gets the image data, which is returned in a 3-frame array
% of r,g,b intensities.
image_data = fitsread (file_path);

% Data values in the FITS files are unsigned integers, but for some
% reason, the fitsread() function interprets them as signed integers.
% Convert any negative values into the appropriate value
image_data(image_data < 0) = image_data(image_data < 0) + 32768;

% To convert the r,g,b values to luminosity, take the max value
% of r,g,b at each pixel.  There are several approaches to r,g,b
% conversion, e.g., http://gimp-savvy.com/BOOK/index.html?node54.html
image_luminosity = max (image_data, [], 3);