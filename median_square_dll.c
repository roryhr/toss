#include "mex.h"

#define NR_END 1

static float stemp;
#define SWAP(a, b) stemp = (a); (a) = (b); (b) = stemp;
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#define MIN(x,y) ((x) < (y) ? (x) : (y))

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	float select(unsigned long k, unsigned long n, float arr[]);
	float *fvector(long nl, long nh);
	void free_fvector(float *v, long nl, long nh);

	int i_row, k_row, n_rows, j_col, l_col, n_cols, row_start, row_end;
	int col_start, col_end, col_skip, kernel_radius, M;
	long n_pixels, kernel_area, kernel_count;
 	float *kernel_vector;
	double *image_luminosity, *out_array;
	bool bEven;

	//-- Get matrix x
	image_luminosity = mxGetPr (prhs[0]);
	n_rows = mxGetN (prhs[0]);
	n_cols = mxGetM (prhs[0]);
	n_pixels = n_rows * n_cols;
	kernel_radius = (int)(mxGetScalar(prhs[1]));

	//Allocate memory and assign output pointer
	plhs[0] = mxCreateDoubleMatrix(n_cols, n_rows, mxREAL); //mxReal is our data-type

	//Get a pointer to the data space in our newly allocated memory
	out_array = mxGetPr(plhs[0]);

	//-- allocate memory for the square around each pixel
	//-- use Numerical Recipes allocation routine because vector[]
	//-- will be passed to select(), which takes the input array as arr[1..nl]
	kernel_area = (2*kernel_radius + 2) * (2*kernel_radius + 2);
	kernel_vector = fvector(1, kernel_area);

	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {

			//-- Decide where the starting and ending rows should be
			row_start = MAX (0, i_row - kernel_radius - 1);
			row_end = MIN (i_row + kernel_radius + 1, n_rows);

			//-- Decide where the starting and ending columns should be
			col_start = MAX (0, j_col - kernel_radius - 1);
			col_end = MIN (j_col + kernel_radius + 1, n_cols);
			
			//-- March over the kernel area and store the pixel values into
			//-- the 1-D kernel storage array that will be passed to select()
			kernel_count = 0;
			for (k_row = row_start; k_row < row_end; k_row++) {
				col_skip = k_row * n_cols;
				for (l_col = col_start; l_col < col_end; l_col++) {
					kernel_count++;
					kernel_vector[kernel_count] = image_luminosity[col_skip + l_col];
				}
			}

			bEven = !(kernel_count % 2);

			M = kernel_count;
			M >>= 1;
			if (!bEven) M++;
			out_array[(i_row*n_cols)+j_col] = (double) select(M, kernel_count, kernel_vector);

			if (bEven) { // if even, take the average of Mth and (M+1)th
				out_array[(i_row*n_cols)+j_col] = (double) (out_array[(i_row*n_cols)+j_col] + select(M + 1, kernel_count, kernel_vector)) / 2.0;
			}
	
		}
	}

	free_fvector (kernel_vector, 1, kernel_area);

	return;
}
/*
void main()
{
	float select(unsigned long k, unsigned long n, float arr[]);
	float *fvector(long nl, long nh);
	void free_fvector(float *v, long nl, long nh);

	int i_row, k_row, n_rows, j_col, l_col, n_cols, row_start, row_end;
	int col_start, col_end, col_skip, kernel_radius, M;
	long n_pixels, kernel_area, kernel_count;
 	float *kernel_vector;
	double image_luminosity[25000], out_array[25000];
	bool bEven;
	
	n_rows = 100;
	n_cols = 250;
	
	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {
			image_luminosity[(i_row*n_cols)+j_col] = (double)((i_row*n_cols)+j_col);
		}
	}

	//-- Get matrix x
	n_cols = 75;
	n_rows = 50;
	n_pixels = n_rows * n_cols;
	kernel_radius = 10;

	//-- allocate memory for the square around each pixel
	//-- use Numerical Recipes allocation routine because vector[]
	//-- will be passed to select(), which takes the input array as arr[1..nl]
	kernel_area = (2*kernel_radius + 2) * (2*kernel_radius + 2);
	kernel_vector = fvector(1, kernel_area);

	for (i_row = 0; i_row < n_rows; i_row++) {
		for (j_col = 0; j_col < n_cols; j_col++) {

			//-- Decide where the starting and ending rows should be
			row_start = MAX (0, i_row - kernel_radius - 1);
			row_end = MIN (i_row + kernel_radius + 1, n_rows);

			//-- Decide where the starting and ending columns should be
			col_start = MAX (0, j_col - kernel_radius - 1);
			col_end = MIN (j_col + kernel_radius + 1, n_cols);
			
			//-- March over the kernel area and store the pixel values into
			//-- the 1-D kernel storage array that will be passed to select()
			kernel_count = 0;
			for (k_row = row_start; k_row < row_end; k_row++) {
				col_skip = k_row * n_cols;
				for (l_col = col_start; l_col < col_end; l_col++) {
					kernel_count++;
					kernel_vector[kernel_count] = image_luminosity[col_skip + l_col];
				}
			}

			bEven = !(kernel_count % 2);

			M = kernel_count;
			M >>= 1;
			if (!bEven) M++;
			out_array[(i_row*n_cols)+j_col] = (double) select(M, kernel_count, kernel_vector);

			if (bEven) { // if even, take the average of Mth and (M+1)th
				out_array[(i_row*n_cols)+j_col] = (double) (out_array[(i_row*n_cols)+j_col] + select(M + 1, kernel_count, kernel_vector)) / 2.0;
			}
	
		}
	}

	free_fvector (kernel_vector, 1, kernel_area);

	return;
}
*/
//==========================================================================
//
// Function: float select
//
// Purpose : Numerical Recipes Function, Select kth largest member of an
//           array with subscript range arr[1..n]
//           modified to inline float
//
//==========================================================================
float select(unsigned long k, unsigned long n, float arr[])
{
	unsigned long i, ir, j, l, mid;
	float a;

	l = 1;
	ir = n;
	for (;;) {
		if (ir <= l + 1) {
			if (ir == l+1 && arr[ir] < arr[l]) {
				SWAP(arr[l], arr[ir])
			}
			return arr[k];
		} else {
			mid = (l + ir) >> 1;
			SWAP(arr[mid], arr[l + 1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l], arr[ir])
			}
			if (arr[l + 1] > arr[ir]) {
				SWAP(arr[l + 1], arr[ir])
			}
			if (arr[l] > arr[l + 1]) {
				SWAP(arr[l], arr[l + 1])
			}
			i = l + 1;
			j = ir;
			a = arr[l + 1];
			for (;;) {
				//-- is this a bug in Numerical Recipes code?
				//-- for certain data sets, the j index goes
				//-- below one and keeps going???
				//do i++; while (arr[i] < a);
				//do j--; while (arr[j] > a);
				do i++; while ((arr[i] < a) && (i<n));
				do j--; while ((arr[j] > a) && (j>1));
				if (j < i) break;
				SWAP(arr[i], arr[j])
			}
			arr[l + 1] = arr[j];
			arr[j] = a;
			if (j >= k) ir = j-1;
			if (j <= k) l = i;
		}
	}
}

//==========================================================================
//
// Function: int *fvector
// Function: void free_fvector
//
// Purpose : Numerical Recipes Function, allocate a float vector 
//           with subscript range v[nl..nh]
//           from nrutil.c
//
//==========================================================================
float *fvector(long nl, long nh)
{
	float *v;
 	char errorMsg[512];

	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	if (!v) {
		return NULL;
	}
	return (v+NR_END-nl);
}

void free_fvector(float *v, long nl, long nh)
{
	nh;
	free((char*) (v+nl-NR_END));
}

#undef SWAP
#undef MIN
#undef MAX
#undef NR_END
