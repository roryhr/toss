function aligned_objects = align_objects (observed_objects, expected_objects, ra_range, dec_range, station_info, filename)

% First thing to do is convert the pixel coordinates of the observed objects
% in the image to a nominal RA/Dec.
% The 'centroids' passed to this routine are in pixel (x,y) coordinates in the image.

% NOTE 04/21/2008: The inversion in RA accounts for the clockwise-from-above designation
% The lack of inversion in Dec is due to the conversion being done when the image is converted
% from .crw to .fit, using pamflip -tb from the netpbm library.  These conversions produce
% valid alignments, provided the pixel_pitch_y parameter corresponds to the physical telescope
% value
pixel_pitch_x = station_info.pixel_pitch_y / abs (cos (station_info.station_lat*pi/180));
observed_objects.ra = ra_range(2) - station_info.pixel_pitch_x*observed_objects.centroid_x;
observed_objects.dec = dec_range(1) + station_info.pixel_pitch_y*observed_objects.centroid_y;

n_observed = size (observed_objects.ra);
n_expected = size (expected_objects.ra);

% One approach to the alignment would be to select the brightest objects
% and perform the alignment only with those objects.
% Matlab will sort ascending by default, so sorting by estimated magnitude
% will put the brightest objects in the lower indices of the array
[sorted, sorted_indices] = sort(observed_objects.estimated_mag);
observed_objects.ra = observed_objects.ra(sorted_indices);
observed_objects.dec = observed_objects.dec(sorted_indices);
observed_objects.estimated_mag = sorted;

[sorted, sorted_indices] = sort(expected_objects.mag);
expected_objects.ra = expected_objects.ra(sorted_indices);
expected_objects.dec = expected_objects.dec(sorted_indices);
expected_objects.mag = sorted;

% Choose subsets of the sorted arrays to send to the alignment algorithm
n_observed_sorted = min (size (observed_objects.ra), 50);
n_expected_sorted = min (size (expected_objects.ra), 100*n_observed);

expected_objects_sort.ra = expected_objects.ra(1:n_expected_sorted(2));
expected_objects_sort.dec = expected_objects.dec(1:n_expected_sorted(2));
expected_objects_sort.mag = expected_objects.mag(1:n_expected_sorted(2));
observed_objects_sort.ra = observed_objects.ra(1:n_observed_sorted(2));
observed_objects_sort.dec = observed_objects.dec(1:n_observed_sorted(2));
observed_objects_sort.estimated_mag = observed_objects.estimated_mag(1:n_observed_sorted(2));

%============================================================================================
% Debug: 
% Write the sorted subset of objects to a disk file for checking the align_objects_dll routine
output_file_path = sprintf ('%s\\data\\debug_data\\%s_observed_objects.csv', station_info.toss_base_dir, filename);
fid = fopen (output_file_path,'w');
for i = 1:n_observed_sorted(2)
    fprintf (fid, '%d,%d,%d\n',observed_objects.ra(i),observed_objects.dec(i),observed_objects.estimated_mag(i));
end
fclose (fid);
output_file_path = sprintf ('%s\\data\\debug_data\\%s_expected_objects.csv', station_info.toss_base_dir, filename);
fid = fopen (output_file_path,'w');
for i = 1:n_expected_sorted(2)
    fprintf (fid, '%d,%d,%d\n',expected_objects.ra(i),expected_objects.dec(i),expected_objects.mag(i));
end
fclose (fid);
% aligned_objects = -1;
% return;
%============================================================================================

% The two patterns should be able to line up when one pattern is laid on top of the other.
% When the patterns are eventually aligned, all of the objects in the observed_objects list 
% will (hopefully) be situated very close to one of the objects in the expected_objects
% list.  
% This suggests an approach for locating the best alignment using nearest-neighbor distance 
% as a metric.  Shift the observed objects so that the first observed object is aligned with 
% one of the template objects, and calculate the total nearest-neighbor distance.  Then, shift 
% the observed objects so that the first observed object is aligned with the next template 
% object, and so on.  The best alignment should occur when the smallest nearest-neighbor 
% distance occurs.  Then, repeat this process for each of the objects in the observed_objects
% list.  The global min will give the best of all possible initial alignments.
[shift_ra, shift_dec, nn_indexes] = align_objects_dll (observed_objects_sort.ra, observed_objects_sort.dec, ...
                                           expected_objects_sort.ra, expected_objects_sort.dec);

% The align_objects_dll routine returns the optimal shift for aligning the two patterns
observed_objects.ra = observed_objects.ra + shift_ra;
observed_objects.dec = observed_objects.dec + shift_dec;

% The align_objects_dll routine also returns the indices of the expected objects which are
% associated with each observed object.  Extract these objects to create the 'template' which
% which the observed objects should be aligned to.
template_objects.ra = expected_objects.ra(nn_indexes);
template_objects.dec = expected_objects.dec(nn_indexes);

%============================================================================================
% Debug: 
output_file_path = sprintf ('%s\\data\\debug_data\\%s_translated_objects.csv', station_info.toss_base_dir, filename);
fid = fopen (output_file_path,'w');
for i = 1:n_observed(2)
    fprintf (fid, '%d,%d,%d\n',observed_objects.ra(i),observed_objects.dec(i),observed_objects.estimated_mag(i));
end
fprintf (fid, '\n');
for i = 1:n_expected(2)
    fprintf (fid, '%d,%d,%d\n',expected_objects.ra(i),expected_objects.dec(i),expected_objects.mag(i));
end
fclose (fid);
%============================================================================================

% The minimum nearest-neighbor distance was determined using x-y translation only.
% The telescope system may not be precisely aligned so that the pixel field is square
% with the RA-Dec grid.  A rotation may put the observed objects into better alignment
% with the expected objects.  A least-squares method for determining 'Plate Constants'
% is employed here.  This method pairs up the observed objects with their corresponding
% expected objects, and minimizes the nearest-neighbor total distance using x-y
% translation and rotation.

% Eliminate outliers before determining the least-squares transformation
nn_distances = sqrt ( (observed_objects.ra - template_objects.ra).^2 ...
                      + (observed_objects.dec - template_objects.dec).^2);

template_objects.ra = template_objects.ra(nn_distances <= station_info.nn_outlier_cutoff);
template_objects.dec = template_objects.dec(nn_distances <= station_info.nn_outlier_cutoff);
associated_objects.ra = observed_objects.ra(nn_distances <= station_info.nn_outlier_cutoff);
associated_objects.dec = observed_objects.dec(nn_distances <= station_info.nn_outlier_cutoff);


%============================================================================================
% A debugging dataset
% template_objects.ra = [68.98000 68.95870 68.78253 68.69320 68.62478 68.62193];
% template_objects.dec = [16.50976 16.41690 16.47306 16.49705 16.51370 16.51958];
% observed_objects.ra = [69.18349 69.14114 68.97047 68.88368 68.81774 68.81500];
% observed_objects.dec = [16.31833 16.21029 16.26130 16.28363 16.29859 16.30366];
% n_observed = size (observed_objects.ra);
% n_expected = size (template_objects.ra);
%============================================================================================

% To determine the best-fit rotation, shift each data set to the put the mean value
% at the origin.  This simplifies the linear system derived from minimizing the mean-
% squared error.  Save the shift amounts for later application.
mean_associated_ra = mean (associated_objects.ra);
mean_associated_dec = mean (associated_objects.dec);
mean_template_ra = mean (template_objects.ra);
mean_template_dec = mean (template_objects.dec);

template_objects.ra = template_objects.ra - mean_template_ra;
template_objects.dec = template_objects.dec - mean_template_dec;
associated_objects.ra = associated_objects.ra - mean_associated_ra;
associated_objects.dec = associated_objects.dec - mean_associated_dec;

% The least-squares linear system for rotation reduces to a 2x2 that can be solved
% directly for the rotation matrix coefficients 
%     A = [ [ a b] 
%           [-b a] ]
denom = sum (associated_objects.ra .* associated_objects.ra) + sum (associated_objects.dec .* associated_objects.dec);
if (denom > 1e-10)
    a = (sum (template_objects.ra .* associated_objects.ra) + sum (template_objects.dec .* associated_objects.dec)) / denom;
    b = (sum (template_objects.ra .* associated_objects.dec) - sum (template_objects.dec .* associated_objects.ra)) / denom;
else
    a = 1;
    b = 0;
end
% theta = atan (b/a);
% a = cos (theta);
% b = sin (theta);

% Once the least-squares coefficients are found, apply the transformation to all of
% the original points in the observed objects list.
% 1. Shift the translated objects to the origin
% 2. Rotate
% 3. Shift back to the template mean
observed_objects.ra = observed_objects.ra - mean_associated_ra;
observed_objects.dec = observed_objects.dec - mean_associated_dec;
for i = 1:n_observed(2)
    aligned_objects.ra(i) = (a * observed_objects.ra(i) + b * observed_objects.dec(i)) + mean_template_ra;
    aligned_objects.dec(i) = (-b * observed_objects.ra(i) + a * observed_objects.dec(i)) + mean_template_dec;
    aligned_objects.intensity(i) = observed_objects.intensity(i);
    aligned_objects.estimated_mag(i) = observed_objects.estimated_mag(i);
end

% For the final list of aligned objects, there should be a determination of
% whether the observed object corresponds to a catalog object or not.  To accomplish
% this association, use the catalog nearest neighbors to each of the aligned
% objects, and apply a unique threshold for correspondence.
for i = 1:n_observed(2)
    nn_dist= sqrt ((aligned_objects.ra(i) - expected_objects.ra(nn_indexes(i)))^2 ...
                      + (aligned_objects.dec(i) - expected_objects.dec(nn_indexes(i)))^2);
    if (nn_dist <= station_info.nn_association_cutoff)
        aligned_objects.id1(i) = expected_objects.id1(nn_indexes(i));
        aligned_objects.id2(i) = expected_objects.id1(nn_indexes(i));
    else
        aligned_objects.id1(i) = -99999;
        aligned_objects.id2(i) = 100000 + i;
    end
    if (aligned_objects.ra(i) > 360)
        aligned_objects.ra(i) = aligned_objects.ra(i) - 360;
    end
end

%============================================================================================
% Debug: 
output_file_path = sprintf ('%s\\data\\debug_data\\%s_aligned_objects.csv', station_info.toss_base_dir, filename);
fid = fopen (output_file_path,'w');
for i = 1:n_observed(2)
    fprintf (fid, '%d,%d,%d,%d,%d,%d\n',aligned_objects.ra(i),aligned_objects.dec(i),aligned_objects.intensity(i), ...
             expected_objects.ra(nn_indexes(i)),expected_objects.dec(nn_indexes(i)),expected_objects.mag(nn_indexes(i)));
end
fclose (fid);
%============================================================================================

scatter (expected_objects.ra, expected_objects.dec)
hold on
scatter (aligned_objects.ra, aligned_objects.dec, 'r+')

return